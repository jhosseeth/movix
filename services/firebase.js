import firebase from 'firebase/app';
import 'firebase/auth';

const config = {
	apiKey: "AIzaSyDfa6C3KrjCPaf06f08hVAhWbfiTjqpj5c",
	authDomain: "movix-f157d.firebaseapp.com",
	databaseURL: "https://movix-f157d.firebaseio.com",
	projectId: "movix-f157d",
	storageBucket: "movix-f157d.appspot.com",
	messagingSenderId: "997395906790",
	appId: "1:997395906790:web:a6af7b94ff8cee42b4bb27"
};

!firebase.apps.length ? firebase.initializeApp(config) : '';

export const auth = firebase.auth();