const axios = require('axios');

const config = {
	language: "es-LA",
	base_uri: "http://api.themoviedb.org/3/",
	images_uri: "http://image.tmdb.org/t/p/",
	api_key: "bf226af439b327f193f382fe8c230d31"
};

export const themoviedb = {
	/**
	* Get the most newly created movie.
	*
	* @return {Object} Last movie info
	*/
	async getLatestMovies() {
		let path = "movie/latest";
		return await this.getData(path);
	},
	/**
	* Get a list of movies in theatres
	*
	* @return {Array} Movies
	*/
	async getTrendingMovies() {
		let path = "movie/now_playing";
		return await this.getList(path);
	},
	/**
	* Get a list of the current popular movies on TMDb.
	*
	* @return {Array} Movies
	*/
	async getPopularMovies() {
		let path = "movie/popular";
		return await this.getList(path);
	},
	/**
	* Get the top rated movies on TMDb.
	*
	* @return {Array} Movies
	*/
	async getTopRatedMovies() {
		let path = "movie/top_rated";
		return await this.getList(path);
	},
	/**
	* Get a list of upcoming movies in theatres
	*
	* @return {Array} Movies
	*/
	async getUpcomingMovies() {
		let path = "movie/upcoming";
		return await this.getList(path);
	},
	/**
	* Get the most newly created TV show.
	*
	* @return {Object} Last TV show info
	*/
	async getLatestTV() {
		let path = "tv/latest";
		return await this.getData(path);
	},
	/**
	* Get a list of shows that are currently on the air.
	*
	* @return {Array} TV shows
	*/
	async getOnAirTV() {
		let path = "tv/on_the_air";
		return await this.getList(path);
	},
	/**
	* Get a list of the current popular TV shows on TMDb.
	*
	* @return {Array} TV shows
	*/
	async getPopularTV() {
		let path = "tv/popular";
		return await this.getList(path);
	},
	/**
	* Get a list of the top rated TV shows on TMDb.
	*
	* @return {Array} TV shows
	*/
	async getTopRatedTV() {
		let path = "tv/top_rated";
        return await this.getList(path);
	},
	/**
	* Get data of the TMDB request.
	*
	* @return {Object}
	*/
	async getData(path) {
		let query = `?api_key=${config.api_key}&language=${config.language}`;
		let data = await axios.get(config.base_uri + path + query)
        .then(function (response) {
            return response.data;
        });

        return data;
	},
	/**
	* Get data list of the TMDB request.
	*
	* @return {Array}
	*/
	async getList(path) {
		let query = `?api_key=${config.api_key}&language=${config.language}`;
		let data = await axios.get(config.base_uri + path + query)
        .then(function (response) {
            return response.data.results;
        });

        return data;
	},
	/**
	* Return the TMDB base url for images.
	*
	* @return {String}
	*/
	getImgBaseURL() {
        return config.images_uri;
	},
}