import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _5d9f2bfe = () => interopDefault(import('..\\pages\\login.vue' /* webpackChunkName: "pages/login" */))
const _269d6c47 = () => interopDefault(import('..\\pages\\movies\\index.vue' /* webpackChunkName: "pages/movies/index" */))
const _d5622080 = () => interopDefault(import('..\\pages\\search.vue' /* webpackChunkName: "pages/search" */))
const _2e12287f = () => interopDefault(import('..\\pages\\tv-shows\\index.vue' /* webpackChunkName: "pages/tv-shows/index" */))
const _58cef603 = () => interopDefault(import('..\\pages\\movies\\search.vue' /* webpackChunkName: "pages/movies/search" */))
const _3ff1c0cb = () => interopDefault(import('..\\pages\\tv-shows\\search.vue' /* webpackChunkName: "pages/tv-shows/search" */))
const _0788c6ea = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/login",
    component: _5d9f2bfe,
    name: "login"
  }, {
    path: "/movies",
    component: _269d6c47,
    name: "movies"
  }, {
    path: "/search",
    component: _d5622080,
    name: "search"
  }, {
    path: "/tv-shows",
    component: _2e12287f,
    name: "tv-shows"
  }, {
    path: "/movies/search",
    component: _58cef603,
    name: "movies-search"
  }, {
    path: "/tv-shows/search",
    component: _3ff1c0cb,
    name: "tv-shows-search"
  }, {
    path: "/",
    component: _0788c6ea,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
