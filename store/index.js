import JWTDecode from "jwt-decode";
import cookieparser from "cookieparser";

export const actions = {

	// Validate cookies when server initialize
	nuxtServerInit({ commit }, { req }) {
		if (process.server && process.static) return; // If is an static web
		if (!req.headers.cookie) return; // If there is not a cookie

		const parsed = cookieparser.parse(req.headers.cookie);
		const accessTokenCookie = parsed.access_token;

		if (!accessTokenCookie) return; // If not exist a token from firebase

		const decoded = JWTDecode(accessTokenCookie);

		if (decoded) {
			commit('users/SET_USER', {
				uid: decoded.user_id,
				email: decoded.email
			})
		}
	}
};